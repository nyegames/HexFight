﻿using Gamelogic.Extensions.Algorithms;
using Gamelogic.Grids;
using Photon.Pun;
using System.Linq;
using UnityEngine;

public class GridBuffController : Photon.Pun.MonoBehaviourPun
{
    public static GridBuffController Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        HexFightGameRound.Instance.ListenPlayerLandNodeCallback((point, playerType) =>
        {
            if (!PhotonNetwork.IsMasterClient) return;

            GameObject playerObj = PlayerController.Instance.GetPlayerObject(playerType);
            if (playerObj == null) return;
            HexPlayer player = playerObj.GetComponent<HexPlayer>();

            PointyHexPoint randomPoint = HexFightGridBehaviour.Instance.HexFightGrid.SampleRandom(1).ToList().First();
            HexFightCell randomCell = HexFightGridBehaviour.Instance.HexFightGrid[randomPoint];
            if (!randomCell.Active) return;
            GameObject gridBuff = PhotonNetwork.Instantiate("Prefabs/GridBuffs/AoEColourSplash", randomCell.transform.position, Quaternion.identity, 0);
            PhotonView gbpv = gridBuff.GetComponent<PhotonView>();

            photonView.RPC("StoreBuff", RpcTarget.AllBuffered, new int[] { randomPoint.X, randomPoint.Y }, gbpv.ViewID);

            PointyHexPoint landPoint = new PointyHexPoint(point[0], point[1]);
            GameObject landedGridBuff = HexFightGridBehaviour.Instance.HexFightGrid[landPoint].GridBuff;
            if (landedGridBuff == null) return;
            PhotonView lpv = landedGridBuff.GetComponent<PhotonView>();
            photonView.RPC("ApplyBuff", RpcTarget.AllBuffered, point, lpv.ViewID, playerType);
        });
    }

    [PunRPC]
    private void StoreBuff(int[] point, int buffPhotonID)
    {
        GameObject gridBuff = PhotonView.Find(buffPhotonID).gameObject;
        PointyHexPoint hexPoint = new PointyHexPoint(point[0], point[1]);
        HexFightGridBehaviour.Instance.HexFightGrid[hexPoint].GridBuff = gridBuff;
    }

    [PunRPC]
    private void ApplyBuff(int[] point, int buffPhotonID, CellProperties.ColourType playerType)
    {
        GameObject gridBuff = PhotonView.Find(buffPhotonID).gameObject;
        PointyHexPoint hexPoint = new PointyHexPoint(point[0], point[1]);
        HexFightGridBehaviour.Instance.HexFightGrid[hexPoint].GridBuff = gridBuff;

        gridBuff.GetComponent<GridBuff>().ApplyBuff(hexPoint, playerType);
    }
}
