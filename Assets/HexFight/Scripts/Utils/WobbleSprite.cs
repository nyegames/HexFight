﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobbleSprite : MonoBehaviour
{
    private enum ScaleState
    {
        UP,
        DOWN
    }

    private ScaleState CurrentState = ScaleState.UP;

    private SpriteRenderer _Sprite;
    private float _Rot = 0;
    // Use this for initialization
    void Start()
    {
        _Sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        _Rot = 250 * Time.deltaTime;
        _Sprite.transform.Rotate(0, 0, _Rot);

        float scaleSpeed = Time.deltaTime * 600;
        switch (CurrentState)
        {
            case ScaleState.UP:
                _Sprite.transform.localScale += Vector3.one * scaleSpeed;
                break;
            case ScaleState.DOWN:
                _Sprite.transform.localScale -= Vector3.one * scaleSpeed;
                break;
        }
        float scaleX = _Sprite.transform.localScale.x;
        if (CurrentState == ScaleState.UP && scaleX > 600) CurrentState = ScaleState.DOWN;
        if (CurrentState == ScaleState.DOWN && scaleX < 300) CurrentState = ScaleState.UP;
    }
}
