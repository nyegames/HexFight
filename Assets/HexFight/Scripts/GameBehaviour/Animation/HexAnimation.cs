﻿using Gamelogic.Grids;
using System;
using System.Linq;
using UnityEngine;

public class HexAnimation : MonoBehaviour
{
    public bool Playing = false;
    public bool Finished = false;

    public enum AnimationType
    {
        SCALE_DOWN,
        SCALE_UP,
        NONE
    }
    public AnimationType HexAnimatonType = AnimationType.NONE;

    public float AnimationSpeed = 10.0f;

    /// <summary>The data object to hold all the informaton on what the winchain is</summary>
    public HexFightGridBehaviour.WinChain WinChain;
    /// <summary>If this isn't null then you are positioning this object on top of each grid square that is animated</summary>
    public bool PlayerToMove;

    private int _WinChainIndex;

    private Action _FinishAction;

    /// <summary>Shortcut to grab the current node you are animating</summary>
    private HexFightCell CurrentHexNode
    {
        get
        {
            return HexFightGridBehaviour.Instance.HexFightGrid[WinChain.PointChain[_WinChainIndex]];
        }
    }
    /// <summary>Shortcut to grab the current point of the node you are animating</summary>
    private PointyHexPoint CurrentHexPoint
    {
        get
        {
            return _WinChainIndex < WinChain.PointChain.Count ? WinChain.PointChain[_WinChainIndex] : WinChain.PointChain.Last();
        }
    }

    /// <summary>Shortcut for grabing the current node you are animating's colour type it will change to after the animation has finished</summary>
    private CellProperties.ColourType CurrentHexType
    {
        get
        {
            return WinChain.SwitchToType[_WinChainIndex];
        }
    }

    /// <summary>Will grab the next point to be animated, if there is no next will return the last</summary>
    private PointyHexPoint NextHexPoint
    {
        get
        {
            return WinChain.PointChain[_WinChainIndex + 1 >= WinChain.PointChain.Count ? _WinChainIndex : _WinChainIndex + 1];
        }
    }

    public void StartAnimation(Action finishAction)
    {
        _FinishAction = finishAction;
        Playing = true;
        HexAnimatonType = AnimationType.SCALE_DOWN;

        foreach (PointyHexPoint point in WinChain.PointChain)
        {
            HexFightGridBehaviour.Instance.HexFightGrid[point].Active = false;
        }
    }

    // Update is called once per frame
    public void HexUpdate(float deltaTime)
    {
        if (!Playing) return;

        GameObject node = GetNodeFromWinChain(WinChain);

        switch (HexAnimatonType)
        {
            case AnimationType.SCALE_DOWN:
                node.transform.localScale -= (Vector3.one * AnimationSpeed) * deltaTime;
                if (node.transform.localScale.x <= 0)
                {
                    CurrentHexNode.CurrentColourType = CurrentHexType;
                    HexAnimatonType = AnimationType.SCALE_UP;
                }
                break;
            case AnimationType.SCALE_UP:
                node.transform.localScale += (Vector3.one * AnimationSpeed) * deltaTime;
                if (node.transform.localScale.x >= 1f)
                {
                    if (PlayerToMove)
                    {
                        //Remove the old position it was at
                        CurrentHexNode.PlayerOnCell = null;
                    }

                    node.transform.localScale = Vector3.one;

                    CurrentHexNode.Active = true;

                    if (PlayerToMove)
                    {
                        HexPlayer player = PlayerController.Instance.GetPlayerObject(WinChain.OwnerType).GetComponent<HexPlayer>();
                        player.SnapToPosition(CurrentHexPoint);

                        CurrentHexNode.PlayerOnCell = PlayerController.Instance.GetPlayerObject(WinChain.OwnerType).GetComponent<HexPlayer>();

                        //Tell all of the clients that this player has landed on this node
                        HexFightGameRound.Instance.PlayerLandedOnNode(new int[] { CurrentHexPoint.X, CurrentHexPoint.Y }, (int)WinChain.OwnerType);
                    }

                    _WinChainIndex++;

                    HexAnimatonType = AnimationType.SCALE_DOWN;
                    if (_WinChainIndex >= WinChain.PointChain.Count)
                    {
                        HexAnimatonType = AnimationType.NONE;
                        if (_FinishAction != null) _FinishAction.Invoke();
                        _FinishAction = null;
                        Playing = false;
                    }
                }
                break;
        }
    }

    private GameObject GetNodeFromWinChain(HexFightGridBehaviour.WinChain winChain)
    {
        return HexFightGridBehaviour.Instance.HexFightGrid[WinChain.PointChain[_WinChainIndex]].gameObject;
    }
}
