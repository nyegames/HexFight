﻿using Gamelogic.Grids;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

public class HexAnimationController : Photon.Pun.MonoBehaviourPun
{
    public static HexAnimationController Instance;

    public void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    public List<HexAnimation> ActiveAnimations = new List<HexAnimation>();

    // Use this for initialization
    void Start()
    {

    }

    public void StartAnimation(HexFightGridBehaviour.WinChain winChain, bool playerToMove)
    {
        HexAnimation hexAnim = gameObject.AddComponent<HexAnimation>();
        hexAnim.transform.parent = transform;

        hexAnim.WinChain = winChain;

        hexAnim.PlayerToMove = playerToMove;

        hexAnim.StartAnimation(() =>
        {
            //Only call this action on the master
            if (!PhotonNetwork.IsMasterClient) return;
            HexFightGameRound.Instance.photonView.RPC("FinishPlayerTurn", RpcTarget.MasterClient, (int)winChain.OwnerType);
        });

        ActiveAnimations.Add(hexAnim);
    }

    // Update is called once per frame
    void Update()
    {
        List<HexAnimation> runningAnimations = new List<HexAnimation>();
        for (int i = 0; i < ActiveAnimations.Count; i++)
        {
            ActiveAnimations[i].HexUpdate(Time.deltaTime);
        }
    }

    [PunRPC]
    public void StartAnimation(int ownerType, int[] originPoint, int[][] chainPositions, int[] switchTypes, int clickedType, bool movePlayer)
    {
        HexFightGridBehaviour.WinChain winChain = new HexFightGridBehaviour.WinChain();

        winChain.OwnerType = (CellProperties.ColourType)ownerType;

        winChain.OriginPoint = new PointyHexPoint(originPoint[0], originPoint[1]);

        for (int i = 0; i < chainPositions.Length; i++)
        {
            winChain.PointChain.Add(new PointyHexPoint(chainPositions[i][0], chainPositions[i][1]));
            winChain.SwitchToType.Add((CellProperties.ColourType)switchTypes[i]);
        }

        winChain.ClickedType = (CellProperties.ColourType)clickedType;

        StartAnimation(winChain, movePlayer);
    }

}
