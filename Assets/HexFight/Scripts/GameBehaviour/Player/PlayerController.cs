﻿using Gamelogic.Grids;
using Photon.Pun;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : Photon.Pun.MonoBehaviourPun
{
    public static PlayerController Instance;

    public void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    public Dictionary<CellProperties.ColourType, string> ActivePlayerIDs = new Dictionary<CellProperties.ColourType, string>();
    public Dictionary<CellProperties.ColourType, int> ActivePhotonIDs = new Dictionary<CellProperties.ColourType, int>();

    public void Start()
    {

    }

    public void RemovePlayer(CellProperties.ColourType type)
    {
        ActivePlayerIDs.Remove(type);
        ActivePhotonIDs.Remove(type);
    }

    private CellProperties.ColourType DeterminePlayerColour()
    {
        List<CellProperties.ColourType> availableColours = new List<CellProperties.ColourType>();
        for (int i = 0; i < (int)CellProperties.ColourType.TOTAL_COLOURS; i++)
        {
            CellProperties.ColourType col = (CellProperties.ColourType)i;
            if (!ActivePlayerIDs.Keys.ToList().Contains(col)) availableColours.Add(col);
        }

        return availableColours[Random.Range(0, availableColours.Count)];
    }

    public GameObject CreatePlayer(CellProperties.ColourType colType, Vector3 position, int[] gridPos)
    {
        GameObject player = PhotonNetwork.Instantiate("Prefabs/Players/Player", Vector3.zero, Quaternion.identity, 0);
        PhotonView ppv = player.GetPhotonView();

        player.transform.position = position;

        HexPlayer playType = player.GetComponent<HexPlayer>();
        playType.PlayerType = colType;
        playType.GridPoint = new PointyHexPoint(gridPos[0], gridPos[1]);

        player.name = "Player_" + colType.ToString();
        return player;
    }

    public GameObject GetPlayerObject(CellProperties.ColourType colourType)
    {
        int photonID = 0;
        if (ActivePhotonIDs.TryGetValue(colourType, out photonID))
        {
            return PhotonView.Find(photonID).gameObject;
        }
        return null;
    }

    private PointyHexPoint[] SpawnPoints =
    {
        new PointyHexPoint(0, 0),
        new PointyHexPoint(13, 0),
        new PointyHexPoint(-4, 8),
        new PointyHexPoint(9, 8)
    };

    [PunRPC]
    public void RequestPlayer(string myPhotonPlayerID)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        CellProperties.ColourType colType = DeterminePlayerColour();

        PointyHexPoint creationPoint = new PointyHexPoint();

        IGrid<HexFightCell, PointyHexPoint> hexGrid = HexFightGridBehaviour.Instance.HexFightGrid;

        foreach (PointyHexPoint spawnPoint in SpawnPoints)
        {
            if (hexGrid[spawnPoint].PlayerOnCell != null) continue;
            creationPoint = spawnPoint;
            break;
        }

        //Set the grid square where the player is spawning to the colour of the player
        hexGrid[creationPoint].CurrentColourType = colType;

        Vector3 startingPos = hexGrid[creationPoint].transform.position;
        int[] gridPoint = new int[] { creationPoint.X, creationPoint.Y };

        GameObject myPlayer = GameObject.Find("MyPlayer");
        PhotonView photonView = myPlayer.GetComponent<PhotonView>();
        Photon.Realtime.Player targetPlayer = PhotonNetwork.PlayerList.First(s => s.UserId == myPhotonPlayerID);
        photonView.RPC("ReceivePlayer", targetPlayer,
            myPhotonPlayerID, colType, startingPos, gridPoint);
    }

    [PunRPC]
    public void RegisterPlayerInfo(int colType, int playerObjPhotonID, string photonPlayerID, int[] gridPos)
    {
        CellProperties.ColourType colourType = (CellProperties.ColourType)colType;

        ActivePlayerIDs.Add(colourType, photonPlayerID);
        ActivePhotonIDs.Add(colourType, playerObjPhotonID);

        GameObject playerObj = PhotonView.Find(playerObjPhotonID).gameObject;
        HexPlayer player = playerObj.GetComponent<HexPlayer>();
        player.PlayerType = colourType;
        player.GridPoint = new PointyHexPoint(gridPos[0], gridPos[1]);

        //Mark the player as on the cell
        HexFightGridBehaviour.Instance.HexFightGrid[player.GridPoint].PlayerOnCell = player;

        //Find each GameObject storing a player and rename it to Player_[COLOUR_TYPE]

        GameObject[] objs = SceneManager.GetActiveScene().GetRootGameObjects();

        //For every base object in the scene
        for (int i = 0; i < objs.Length; i++)
        {
            //Does it contain a player?
            GameObject baseObj = objs[i];
            HexPlayer ply = baseObj.GetComponent<HexPlayer>();
            if (ply == null) continue;

            ply.gameObject.name = "Player_" + ply.PlayerType.ToString();

        }

        DisconnectionController.Instance.photonView.RPC("RegisterPlayerObject", RpcTarget.AllBuffered,
            ActivePlayerIDs[colourType], colType, playerObjPhotonID);

        HexFightGameRound.Instance.photonView.RPC("PlayersConnectedUpdate", RpcTarget.AllBuffered, PhotonNetwork.PlayerList.Length);
    }
}
