﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class PregameCountdown : Photon.Pun.MonoBehaviourPun
{
    public static PregameCountdown Instance;

    private bool _Finished = false;
    private bool CountingDown = false;

    private float CurrentTime = 60;

    private Text PreGameCountDown;

    // Use this for initialization
    void Start()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        PreGameCountDown = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!CountingDown || _Finished) return;
        CurrentTime -= Time.deltaTime;
        if (PhotonNetwork.IsMasterClient) photonView.RPC("UpdateValue", RpcTarget.All, CurrentTime);
        if (CurrentTime > 0f) return;
        _Finished = true;
    }

    /// <summary>
    /// Start the count down on the master client count down clock
    /// </summary>
    /// <param name="value"></param>
    /// <param name="startTime"></param>
    [PunRPC]
    public void CountDown(bool value, float startTime)
    {
        CountingDown = value;
        CurrentTime = startTime;
        if (!CountingDown) UpdateValue(startTime);
    }

    [PunRPC]
    public void UpdateValue(float value)
    {
        PreGameCountDown.text = ((int)value).ToString();
        if (value <= 0 && PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("HideCountdown", RpcTarget.AllBuffered);
            HexFightGameRound.Instance.photonView.RPC("StartGame", RpcTarget.All);
        }
    }

    [PunRPC]
    public void HideCountdown()
    {
        gameObject.SetActive(false);
    }
}
